package com.petrovsky.ssta.repository;

import com.petrovsky.ssta.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Class User Repository
 * Repository that holds all user's requests to the database
 *
 * @author Sergey Petrovsky
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByName(String name);
}

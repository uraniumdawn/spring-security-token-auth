package com.petrovsky.ssta.controller;

import com.petrovsky.ssta.model.Authority;
import com.petrovsky.ssta.model.User;
import com.petrovsky.ssta.security.JwtTokenUtil;
import com.petrovsky.ssta.security.model.AuthenticationRequest;
import com.petrovsky.ssta.security.model.AuthenticationResponse;
import com.petrovsky.ssta.security.service.UserDetailsServiceImpl;
import com.petrovsky.ssta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class UserController
 * REST Controller that handles all user requests
 *
 * @author Sergey Petrovsky
 */
@RestController
public class UserController {

    @Value("${token.key}")
    private String tokenKey;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/api/auth", method = RequestMethod.POST)
    public void createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest, HttpServletResponse response) throws AuthenticationException {

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                authenticationRequest.getUsername(),
                authenticationRequest.getPassword()
        );

        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        final UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken((User) userDetails);

        Cookie cookie = new Cookie(tokenKey, token);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    @RequestMapping(value = "/api/user/get/current", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public User getCurrentUser(HttpServletRequest httpRequest) {
        return jwtTokenUtil.parseToken(WebUtils.getCookie(httpRequest, tokenKey).getValue());
    }

    @RequestMapping(value = "/api/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<User> getUsers() throws AuthenticationException {
        return userService.getUsers();
    }

    @RequestMapping(value = "/api/init", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void initUsers() throws AuthenticationException {

        User user = new User();
        user.setName("user");
        user.setPassword(passwordEncoder.encode("user"));
        user.setEnabled(true);

        Authority userAuthority = new Authority();
        userAuthority.setAuthority("USER");
        Set<Authority> userAuthorities = new HashSet<>();
        userAuthorities.add(userAuthority);

        user.setAuthorities(userAuthorities);
        userService.saveUser(user);

        User admin = new User();
        admin.setName("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setEnabled(true);

        Authority adminAuthority = new Authority();
        adminAuthority.setAuthority("ADMIN");
        Set<Authority> adminAuthorities = new HashSet<>();
        adminAuthorities.add(adminAuthority);

        admin.setAuthorities(adminAuthorities);
        userService.saveUser(admin);
    }
}

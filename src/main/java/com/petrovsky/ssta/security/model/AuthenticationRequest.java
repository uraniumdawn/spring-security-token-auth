package com.petrovsky.ssta.security.model;

/**
 * Class AuthenticationRequest
 * It is model Entity, that not store in database
 * and consists data of authorization request
 *
 * @author Sergey Petrovsky
 */
public class AuthenticationRequest {

    private String username;
    private String password;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

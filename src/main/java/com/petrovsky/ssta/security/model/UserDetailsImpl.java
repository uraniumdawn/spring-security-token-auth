package com.petrovsky.ssta.security.model;

import com.petrovsky.ssta.model.Authority;
import com.petrovsky.ssta.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;

/**
 * Class UserDetailsImpl
 * Class that provides core user information witch encapsulated into Authentication objects.
 *
 * @author Sergey Petrovsky
 */
public class UserDetailsImpl extends User implements UserDetails {

    public UserDetailsImpl(User user) {
        super(user);
    }

    @Override public String getUsername() {
        return super.getName();
    }
    @Override public String getPassword() {
        return super.getPassword();
    }
    @Override public boolean isAccountNonExpired() {return true;}
    @Override public boolean isAccountNonLocked() {return true;}
    @Override public boolean isCredentialsNonExpired() {return true;}
    @Override public boolean isEnabled() {
        return super.isEnabled();
    }
    @Override public Set<Authority> getAuthorities() {
        return super.getAuthorities();
    }
}

package com.petrovsky.ssta.security.model;

/**
 * Class AuthenticationResponse
 * It is model Entity, that not store in database
 * and consists data of authorization response
 *
 * @author Sergey Petrovsky
 */
public class AuthenticationResponse {

    private final String token;

    public AuthenticationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}

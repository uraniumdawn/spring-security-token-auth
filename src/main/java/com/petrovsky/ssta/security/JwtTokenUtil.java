package com.petrovsky.ssta.security;

import com.petrovsky.ssta.model.Authority;
import com.petrovsky.ssta.model.User;
import com.petrovsky.ssta.security.model.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**Class JwtTokenUtil
 * It holds functionality of work with authorization token
 *
 * @author Sergey Petrovsky
 */
@Component
public class JwtTokenUtil {

    @Value("${token.secret}")
    private String secret;

    @Value("${token.expiration-time}")
    private Long expiration;

    @Value ("${token.time.zoneId}")
    private String timeZoneId;

    /**
     * Generates a JWT token containing username as subject, userId and authorities as additional claims. These properties are taken from the specified
     * User object.
     *
     * @param user the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(User user) {
        Claims claims = Jwts.claims().setSubject(user.getName());
        claims.put("ID", String.valueOf(user.getId()));
        claims.put("Authorities", user.getAuthorities());

        Long nowMillis = LocalDateTime.now().atZone(ZoneId.of(timeZoneId)).toInstant().toEpochMilli();
        Date expirationDate = new Date(nowMillis + expiration);

        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS25")
                .setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and authorities prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public User parseToken(String token) {

        //todo: remove Exception
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            User user = new User();
            user.setName(body.getSubject());
            user.setId(Long.parseLong((String) body.get("ID")));
            user.setAuthorities(((List<Authority>) body.get("Authorities")).stream().collect(Collectors.toSet()));

            return user;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse and valid for remoteness a JWT token. If successful, returns true. If unsuccessful (token has elapsed time), returns false.
     *
     * @param token the JWT token to parse
     * @return boolean result of token validation.
     */
    public boolean isTokenValid(String token) {

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            return body.getExpiration().before(new Date(LocalDateTime.now().atZone(ZoneId.of(timeZoneId)).toInstant().toEpochMilli()));

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

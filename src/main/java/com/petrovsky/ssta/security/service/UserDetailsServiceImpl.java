package com.petrovsky.ssta.security.service;

import com.petrovsky.ssta.model.User;
import com.petrovsky.ssta.repository.UserRepository;
import com.petrovsky.ssta.security.model.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * UserDetailsService
 * Service that implements UserDetailsService interface for loads user-specific data
 *
 * @author Sergey Petrovsky
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    /**
     * Method loadUserByUsername load user by username
     * @param name by that user should be loaded
     * @return UserDetails for current user
     */
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userRepository.findByName(name);
        if (user == null) throw new UsernameNotFoundException("No user found for name '" + name +"'.");
        return new UserDetailsImpl(user);
    }
}

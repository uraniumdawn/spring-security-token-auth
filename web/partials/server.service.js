ssta.factory('ServerService', ['$http', function ($http) {

    var serverURLs = {
        initUsersGET: '/api/init',
        authentificationPOST: '/api/auth',
        usersGET: '/api/users',
        currentUserRoleGET: '/api/user/get/current'
    };

    return {
        initUsers: function () {
            return $http.get(serverURLs.initUsersGET);
        },

        login: function (credentials) {
            return $http.post(serverURLs.authentificationPOST, credentials);
        },

        getUsers: function () {
            return $http.get(serverURLs.usersGET);
        },

        getCurrentUser: function () {
            return $http.get(serverURLs.currentUserRoleGET);
        }
    };
}]);
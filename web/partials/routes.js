ssta.config(function ($stateProvider, Authorities) {
    $stateProvider

    .state('main', {
        url: '/',
        controller: 'MainController',
        templateUrl: 'partials/main/main.template.html'
    })

    .state('main.admin', {
        resolve: {permission: function(AuthService) {
                return AuthService.permission([Authorities.ADMIN]);
            }
        },
        url: '^/admin',
        controller: 'AdminController',
        templateUrl: 'partials/admin/admin.template.html'
    })

    .state('main.user', {
        resolve: {permission: function(AuthService) {
               return AuthService.permission([Authorities.ADMIN, Authorities.USER]);
            }
        },
        url: '^/user',
        controller: 'UserController',
        templateUrl: 'partials/user/user.template.html'
    })
});
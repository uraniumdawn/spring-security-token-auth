ssta.controller('MainController',
    function ($scope, MainService) {

        $scope.user = {
            username: "",
            password: ""
        };

        $scope.initUsers = function() {
            MainService.initUsers();
        };

        $scope.login = function() {
            MainService.login($scope.user).then(function(promise){

            });
        };

        $scope.logout = function() {
            MainService.logout();
        }
    }
);
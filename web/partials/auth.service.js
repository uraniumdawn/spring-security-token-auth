ssta.constant('Authorities', {
        ADMIN: 'ADMIN',
        USER: 'USER'
    });

ssta.factory('AuthService', function ($q, $http, ServerService) {
    return {
        permission: function (acceptableAuthorities) {
            var defer = $q.defer();
            var authorities = undefined;
            ServerService.getCurrentUser().then(function(promise) {
                authorities = takeAuthority(promise.data.authorities);
                if(containAuthority(acceptableAuthorities, authorities)) {
                    defer.resolve();
                } else {
                    defer.reject();
                }
            });
            return defer.promise;
        }
    };
});

/*Function for converting array which consist element of form {authority: #Authority} to
* array which consist element of form {#Authority}.
* This is necessary for right comparison and finding common authorities*/
function takeAuthority(arr) {
    return arr.map(function (n) {
        return n.authority
    });
}

/*Function that provide finding User's authorities among acceptable authorities*/
function containAuthority(arr1, arr2) {
    return arr1.some(function (n) {
        return arr2.indexOf(n) != -1;
    });
}
